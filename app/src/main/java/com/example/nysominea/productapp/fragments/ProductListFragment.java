package com.example.nysominea.productapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.nysominea.productapp.R;
import com.example.nysominea.productapp.adapters.ProductListAdapter;
import com.example.nysominea.productapp.models.Product;

import java.util.ArrayList;

/**
 * Created by NySominea on 26/02/2018.
 */

public class ProductListFragment extends Fragment{

    private RecyclerView mProductList;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    public ProductListAdapter adapter;
    private ArrayList<Product> productList;

    public ProductListFragment(){}



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_list, container,false);

        mProductList = view.findViewById(R.id.product_list);
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_refresh);

        productList = new ArrayList<>();

        productList.add(new Product("1","Car for sale",10000.00,"","Vehicle","","",null,1));
        productList.add(new Product("2","Car for sale",10000.00,"","Vehicle","","",null,1));
        productList.add(new Product("4","Car for sale",10000.00,"","Vehicle","","",null,1));

        adapter = new ProductListAdapter(this.productList, getActivity());
        mProductList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mProductList.setHasFixedSize(true);
//        mProductList.addItemDecoration();
        mProductList.setAdapter(adapter);
        return view;
    }
}
