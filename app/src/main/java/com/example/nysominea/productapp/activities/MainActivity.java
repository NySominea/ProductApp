package com.example.nysominea.productapp.activities;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.nysominea.productapp.adapters.ViewPagerAdapter;
import com.example.nysominea.productapp.fragments.FavoriteFragment;
import com.example.nysominea.productapp.fragments.ProductListFragment;
import com.example.nysominea.productapp.fragments.RecentFragment;
import com.example.nysominea.productapp.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private ViewPager viewPager;
    private TabLayout tabLayout;

    private ProductListFragment homeFragment;
    private RecentFragment recentFragment;
    private FavoriteFragment favoriteFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(this);

        viewPager = findViewById(R.id.viewpager);
        setUpViewPager(viewPager);

        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    public void setUpViewPager(ViewPager viewPager) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new ProductListFragment(),"Home");
        viewPagerAdapter.addFragment(new RecentFragment(),"Recent");
        viewPagerAdapter.addFragment(new FavoriteFragment(),"Favorite");
        viewPager.setAdapter(viewPagerAdapter);
    }

    @Override
    public void onClick(View view) {
        Intent viewCategory = new Intent(MainActivity.this,CategoryActivity.class);
        startActivity(viewCategory);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.search :
                Intent search = new Intent(MainActivity.this,SearchActivity.class);
                startActivity(search);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
