package com.example.nysominea.productapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.nysominea.productapp.R;

public class CategoryActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Categories");
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        Intent sendBack = new Intent();
        this.setResult(Activity.RESULT_CANCELED,sendBack);
        finish();
    }
}
