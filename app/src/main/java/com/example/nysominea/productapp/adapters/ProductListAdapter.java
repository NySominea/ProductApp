package com.example.nysominea.productapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nysominea.productapp.R;
import com.example.nysominea.productapp.models.Product;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by NySominea on 28/02/2018.
 */

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ProductListHolder> {

    private ArrayList<Product> productList;
    private Context context;

    public ProductListAdapter(ArrayList<Product> productList, Context context){
        this.productList = productList;
        this.context = context;
    }

    @Override
    public ProductListAdapter.ProductListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_product_list,null);
        return new ProductListHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductListHolder holder, int position) {
        Product product = productList.get(position);
        holder.bind(product);
    }


    @Override
    public int getItemCount() {
        return (productList != null) ? productList.size() : 0;
    }

    class ProductListHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView mTitle;
        private TextView mAddress;
        private TextView mPrice;
        private ImageView mImage;
        private Product product;

        public ProductListHolder(View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.txt_title);
            mAddress = itemView.findViewById(R.id.text_address);
            mPrice = itemView.findViewById(R.id.text_price);
            mImage = itemView.findViewById(R.id.image);
            itemView.setOnClickListener(this);
        }

        public  void bind(Product product){
            this.product = product;
            if(this.product != null){
                this.mTitle.setText(product.getTitle());
                this.mAddress.setText(product.getAddress());
                this.mPrice.setText(product.getPrice()+"");
                if (product.getImageUrl() != null){
                    Picasso.with(context).load(product.getImageUrl()).into(this.mImage);
                }
            }
        }

        @Override
        public void onClick(View view) {

        }
    }
}
