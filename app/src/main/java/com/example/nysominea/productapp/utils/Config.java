package com.example.nysominea.productapp.utils;


public class Config {
    private static final Config ourInstance = new Config();

    public static Config getInstance() {
        return ourInstance;
    }

    private Config() {
    }

    public final String API_URL = "http://10.0.2.2:8000/api/";
}
