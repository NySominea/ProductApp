package com.example.nysominea.productapp.utils.retrofit.clients;

import com.example.nysominea.productapp.models.Product;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface NewFeedClient {

    @GET("newfeed")
    Call<List<Product>> getProductFeed();

}
