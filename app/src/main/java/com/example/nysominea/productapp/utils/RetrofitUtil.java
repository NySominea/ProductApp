package com.example.nysominea.productapp.utils;

import com.example.nysominea.productapp.utils.retrofit.clients.NewFeedClient;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitUtil {

    private static RetrofitUtil instance;

    private RetrofitUtil(){

    }

    public static RetrofitUtil getInstance(){
        if(instance == null)
            instance = new RetrofitUtil();

        return instance;
    }

    private String API_BASE_URL = Config.getInstance().API_URL;

    private OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(API_BASE_URL)
                        .addConverterFactory(
                                GsonConverterFactory.create()
                        );

    private Retrofit retrofit =
                builder
                        .client(
                                httpClient.build()
                        )
                        .build();

    private NewFeedClient newFeedClient =  retrofit.create(NewFeedClient.class);

    public NewFeedClient getNewFeedClient(){
        return newFeedClient;
    }

}
